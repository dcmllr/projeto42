
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniele Claudine Muller
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse a = new Elipse(6, 4);
        Circulo b = new Circulo(7);
        double area, perimetro;
        String nome;
        
        area = a.getArea();
        perimetro = a.getPerimetro();
        nome = a.getNome();
        
        System.out.println("Nome: " + nome);
        System.out.println("Area: " + area);
        System.out.println("Perimetro: " + perimetro);
        
        area = b.getArea();
        perimetro = b.getPerimetro();
        nome = b.getNome();
        
        System.out.println("Nome: " + nome);
        System.out.println("Area: " + area);
        System.out.println("Perimetro: " + perimetro);
    }
}
